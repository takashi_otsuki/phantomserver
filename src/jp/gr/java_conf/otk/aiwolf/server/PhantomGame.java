package jp.gr.java_conf.otk.aiwolf.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Guard;
import org.aiwolf.common.data.Judge;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.data.Status;
import org.aiwolf.common.data.Talk;
import org.aiwolf.common.data.Vote;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.server.GameData;

/**
 * Phantom Game.
 * 
 * @author Takashi OTSUKI
 *
 */
public class PhantomGame {
	/**
	 * Queue containing log.
	 */
	private LinkedList<String> logQueue = new LinkedList<String>();

	private GameSetting gameSetting;
	private PhantomServer server;
	private GameData gameData;

	/**
	 * Map between day and game data.
	 */
	private TreeMap<Integer, GameData> gameDataMap = new TreeMap<Integer, GameData>();

	private HashMap<Agent, String> agentNameMap = new HashMap<Agent, String>();
	private HashMap<Agent, Role> agentRoleMap = new HashMap<Agent, Role>();

	/**
	 * Monitor agent.
	 */
	private Agent monitor;

	/**
	 * Initializes a new instance of this class.
	 * 
	 * @param stream
	 *            - Input stream of game log.
	 * @param port
	 *            - Server's port number.
	 */
	public PhantomGame(InputStream stream, int port) {
		server = new PhantomServer(port);
		server.waitForConnection();
		// No need to specify agent for requestName because there's only one running agent.
		String requestName = server.requestName(null);
		// No need to specify agent for requestRequestRole because there's only one running agent.
		Role requestRole = server.requestRequestRole(null);

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		while (readOneGame(reader)) {
			buildAgentMaps();

			monitor = decideMonitorAgent(requestName, requestRole);
			server.setMonitor(monitor);

			gameSetting = GameSetting.getDefaultGame(agentRoleMap.size());
			server.setGameSetting(gameSetting);

			gameDataMap.clear();
			gameData = new GameData(gameSetting);
			for (Agent a : agentRoleMap.keySet()) {
				gameData.addAgent(a, Status.ALIVE, agentRoleMap.get(a));
			}
			gameDataMap.put(gameData.getDay(), gameData);
			server.setGameData(gameData);

			startOneGame();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		server.close();
	}

	/**
	 * Read log of one game.
	 * 
	 * @param reader
	 * @return True on success, otherwise, false.
	 */
	private boolean readOneGame(BufferedReader reader) {
		logQueue.clear();
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				logQueue.offer(line);
				String[] data = line.split(",");
				if (data[1].equals("result")) {
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void buildAgentMaps() {
		// Initialize using lines on day 0.
		agentRoleMap.clear();
		agentNameMap.clear();
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (!data[1].equals("status")) {
				logQueue.addFirst(line);
				break;
			}
			Agent agent = Agent.getAgent(Integer.parseInt(data[2]));
			Role role = Role.valueOf(data[3]);
			agentRoleMap.put(agent, role);
			agentNameMap.put(agent, data[5]);
		}
	}

	private Agent decideMonitorAgent(String requestName, Role requestRole) {
		Agent agent = null;
		// Decide monitor agent.
		if (requestName != null) {
			for (Agent a : agentNameMap.keySet()) {
				if (requestName.equalsIgnoreCase(agentNameMap.get(a))) {
					agent = a;
				}
			}
		}
		if (agent == null) {
			if (requestRole != null) {
				ArrayList<Agent> list = new ArrayList<Agent>();
				for (Agent a : agentRoleMap.keySet()) {
					if (requestRole == agentRoleMap.get(a)) {
						list.add(a);
					}
				}
				Collections.shuffle(list);
				agent = list.get(0);
			} else {
				ArrayList<Agent> list = new ArrayList<Agent>(agentRoleMap.keySet());
				Collections.shuffle(list);
				agent = list.get(0);
			}
		}
		return agent;
	}

	public void startOneGame() {
		server.init(monitor);
		while (!logQueue.isEmpty()) {
			String line;
			while ((line = logQueue.poll()) != null) {
				String[] data = line.split(",");
				if (data[1].equals("status")) {
					continue;
				} else {
					logQueue.addFirst(line);
					break;
				}
			}
			if ((line = logQueue.poll()) != null) {
				String[] data = line.split(",");
				if (data[1].equals("result")) {
					break;
				} else {
					logQueue.addFirst(line);
				}
			}

			// Day
			server.dayStart(monitor);
			if (gameData.getDay() == 0) {
				if (gameSetting.isTalkOnFirstDay()) {
					whisper();
					talk();
				}
			} else {
				talk();
			}

			// Night
			server.dayFinish(monitor);
			if (!gameSetting.isTalkOnFirstDay() && gameData.getDay() == 0) {
				whisper();
			}
			// Vote and execute except day 0
			if (gameData.getDay() != 0) {
				for (int i = 0; i <= gameSetting.getMaxRevote(); i++) {
					vote();
					if (execute()) {
						break;
					}
				}
			}
			// Every day
			divine();
			// whisper, guard, attackVote and attack except day 0
			if (gameData.getDay() != 0) {
				whisper();
				guard();
				for (int i = 0; i <= gameSetting.getMaxAttackRevote(); i++) {
					if (i > 0 && gameSetting.isWhisperBeforeRevote()) { // Re-vote
						whisper();
					}
					attackVote();
					if (attack()) {
						break;
					}
				}
			}
			gameData = gameData.nextDay();
			gameDataMap.put(gameData.getDay(), gameData);
			server.setGameData(gameData);
		}
		server.finish(monitor);
	}

	private void talk() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (data[1].equals("talk")) {
				Talk talk = toTalk(data);
				Agent agent = talk.getAgent();
				if (agent == monitor) {
					// Ignore monitor's talk.
					server.requestTalk(monitor);
				}
				gameData.addTalk(agent, talk);
			} else {
				logQueue.addFirst(line);
				break;
			}
		}
	}

	private void whisper() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (data[1].equals("whisper")) {
				Talk talk = toTalk(data);
				Agent agent = talk.getAgent();
				if (agent == monitor) {
					// Ignore monitor's whisper.
					server.requestWhisper(monitor);
				}
				gameData.addWhisper(agent, talk);
			} else {
				logQueue.addFirst(line);
				break;
			}
		}
	}

	private void vote() {
		List<Vote> latestVoteList = new ArrayList<>();
		List<Agent> voters = new ArrayList<>();
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (!data[1].equals("vote")) {
				logQueue.addFirst(line);
				break;
			}
			Vote vote = toVote(data);
			Agent agent = vote.getAgent();
			if (voters.contains(agent)) { // Re-vote
				logQueue.addFirst(line);
				break;
			}
			voters.add(agent);
			if (agent == monitor) {
				server.requestVote(monitor);
			}
			latestVoteList.add(vote);
		}
		if (latestVoteList.isEmpty()) {
			return; // 投票パートでない
		}
		gameData.setLatestVoteList(latestVoteList);
		gameData.getVoteList().clear();
		for (Vote vote : latestVoteList) {
			gameData.addVote(vote);
		}
	}

	private boolean execute() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (data[1].equals("execute")) {
				gameData.setExecutedTarget(Agent.getAgent(Integer.parseInt(data[2])));
				return true;
			} else {
				logQueue.addFirst(line);
				break;
			}
		}
		return false;
	}

	private void divine() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (!data[1].equals("divine")) {
				logQueue.addFirst(line);
				break;
			}
			Judge divine = toJudge(data);
			Agent agent = divine.getAgent();
			if (agent == monitor) {
				server.requestDivineTarget(monitor);
			}
			gameData.addDivine(divine);
			if (gameData.getRole(divine.getTarget()) == Role.FOX) {
				gameData.addLastDeadAgent(divine.getTarget());
				gameData.setCursedFox(divine.getTarget());
			}
		}
	}

	private void guard() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (!data[1].equals("guard")) {
				logQueue.addFirst(line);
				break;
			}
			Guard guard = toGuard(data);
			Agent agent = guard.getAgent();
			if (agent == monitor) {
				server.requestGuardTarget(monitor);
			}
			gameData.addGuard(guard);
		}
	}

	private void attackVote() {
		List<Vote> latestAttackVoteList = new ArrayList<>();
		List<Agent> voters = new ArrayList<>();
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (!data[1].equals("attackVote")) {
				logQueue.addFirst(line);
				break;
			}
			Vote vote = toVote(data);
			Agent agent = vote.getAgent();
			if (voters.contains(agent)) { // Re-vote
				logQueue.addFirst(line);
				break;
			}
			voters.add(agent);
			if (agent == monitor) {
				server.requestAttackTarget(monitor);
			}
			latestAttackVoteList.add(vote);
		}
		if (latestAttackVoteList.isEmpty()) {
			return;
		}
		gameData.setLatestAttackVoteList(latestAttackVoteList);
		gameData.getAttackVoteList().clear();
		for (Vote vote : latestAttackVoteList) {
			gameData.addAttack(vote);
		}
	}

	private boolean attack() {
		String line;
		while ((line = logQueue.poll()) != null) {
			String[] data = line.split(",");
			if (data[1].equals("attack")) {
				Agent agent = Agent.getAgent(Integer.parseInt(data[2]));
				gameData.setAttackedTarget(agent);
				if (data[3].equals("true")) {
					gameData.addLastDeadAgent(agent);
				}
				return true;
			} else {
				logQueue.addFirst(line);
				break;
			}
		}
		return false;
	}

	private Vote toVote(String[] data) {
		Agent agent = Agent.getAgent(Integer.parseInt(data[2]));
		Agent target = Agent.getAgent(Integer.parseInt(data[3]));
		Vote vote = new Vote(Integer.parseInt(data[2]), agent, target);
		return vote;
	}

	private Talk toTalk(String[] data) {
		Talk talk = new Talk(Integer.parseInt(data[2]), Integer.parseInt(data[0]), Integer.parseInt(data[3]), Agent.getAgent(Integer.parseInt(data[4])), data[5]);
		return talk;
	}

	private Judge toJudge(String[] data) {
		Agent target = Agent.getAgent(Integer.parseInt(data[3]));
		Judge judge = new Judge(Integer.parseInt(data[0]), Agent.getAgent(Integer.parseInt(data[2])), target, gameData.getRole(target).getSpecies());
		return judge;
	}

	private Guard toGuard(String[] data) {
		Agent target = Agent.getAgent(Integer.parseInt(data[3]));
		Agent agent = Agent.getAgent(Integer.parseInt(data[2]));
		Guard guard = new Guard(Integer.parseInt(data[0]), agent, target);
		return guard;
	}

}
