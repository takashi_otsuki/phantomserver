package jp.gr.java_conf.otk.aiwolf.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Request;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.net.DataConverter;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.common.net.Packet;
import org.aiwolf.common.net.TalkToSend;
import org.aiwolf.server.GameData;
import org.aiwolf.server.net.GameServer;

/**
 * Phantom Server.
 * 
 * <pre>
 * PhantomServer is a virtual AIWolf game server which accepts only one connection from agent.
 * It reads AIWolf game log, and acts as if the all agents played according to the log.
 * The connected agent is assigned to the specified one in the log, in other words,
 * it receives the same information as the specified one.
 * However, what it sends to the server is ignored as if it were a phantom.
 * </pre>
 * 
 * @author Takashi OTSUKI
 *
 */
public class PhantomServer implements GameServer {

	private int port;
	private Agent monitor;
	private Socket socket;
	private String name;
	private GameData gameData;
	private GameSetting gameSetting;
	private int lastTalkIdx;
	private int lastWhisperIdx;

	public PhantomServer(int port) {
		this.port = port;
	}

	public void setMonitor(Agent monitor) {
		this.monitor = monitor;
	}

	/**
	 * Waits one connection from agent.
	 */
	public void waitForConnection() {
		try {
			if (socket != null && socket.isConnected()) {
				socket.close();
			}
			ServerSocket serverSocket = new ServerSocket(port);
			socket = serverSocket.accept();
			serverSocket.close();
			// No need to specify agent for requestName because there's only one running agent.
			name = requestName(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Agent> getConnectedAgentList() {
		ArrayList<Agent> list = new ArrayList<Agent>();
		list.add(monitor);
		return list;
	}

	private void send(Request request) {
		String message = null;
		Packet packet;
		if (request == Request.NAME || request == Request.ROLE) {
			packet = new Packet(request);
		} else {
			if (monitor == null) {
				System.err.println("PhantomServer: Monitor agent is null.");
				System.exit(0);
			}
			if (request == Request.INITIALIZE) {
				lastTalkIdx = 0;
				lastWhisperIdx = 0;
				packet = new Packet(request, gameData.getGameInfoToSend(monitor), gameSetting);
			} else if (request == Request.DAILY_INITIALIZE) {
				lastTalkIdx = 0;
				lastWhisperIdx = 0;
				packet = new Packet(request, gameData.getGameInfoToSend(monitor));
			} else if (request != Request.FINISH) {
				// In case of revote for execution and attack, make packet have GameInfo to inform the agent of the latest vote.
				// In the request call after execution, do the same way as above.
				if ((request == Request.VOTE && !gameData.getLatestVoteList().isEmpty()) || (request == Request.ATTACK && !gameData.getLatestAttackVoteList().isEmpty())
						|| (gameData.getExecuted() != null && (request == Request.DIVINE || request == Request.GUARD || request == Request.WHISPER || request == Request.ATTACK))) {
					packet = new Packet(request, gameData.getGameInfoToSend(monitor));
				} else {
					List<TalkToSend> talkList = gameData.getGameInfoToSend(monitor).getTalkList();
					int lastIdx = talkList.size();
					if (lastIdx >= lastTalkIdx) {
						talkList = talkList.subList(lastTalkIdx, lastIdx);
						lastTalkIdx = lastIdx;
					}
					List<TalkToSend> whisperList = gameData.getGameInfoToSend(monitor).getWhisperList();
					lastIdx = whisperList.size();
					if (lastIdx >= lastWhisperIdx) {
						whisperList = whisperList.subList(lastWhisperIdx, lastIdx);
						lastWhisperIdx = lastIdx;
					}
					packet = new Packet(request, talkList, whisperList);
				}
			} else {
				packet = new Packet(request, gameData.getFinalGameInfoToSend(monitor));
			}
		}
		message = DataConverter.getInstance().convert(packet);
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			bw.append(message);
			bw.append("\n");
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Object request(Request request) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		send(request);
		String line = null;
		try {
			line = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (line.isEmpty()) {
			line = null;
		}
		if (request == Request.TALK || request == Request.WHISPER || request == Request.NAME || request == Request.ROLE) {
			return line;
		} else if (request == Request.ATTACK || request == Request.DIVINE || request == Request.GUARD || request == Request.VOTE) {
			return DataConverter.getInstance().toAgent(line);
		} else {
			return null;
		}
	}

	@Override
	public void init(Agent agent) {
		send(Request.INITIALIZE);
	}

	@Override
	public void dayStart(Agent agent) {
		send(Request.DAILY_INITIALIZE);
	}

	@Override
	public void dayFinish(Agent agent) {
		send(Request.DAILY_FINISH);
	}

	@Override
	public String requestName(Agent agent) {
		return name != null ? name : (String) request(Request.NAME);
	}

	@Override
	public Role requestRequestRole(Agent agent) {
		try {
			return Role.valueOf((String) request(Request.ROLE));
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	@Override
	public String requestTalk(Agent agent) {
		return (String) request(Request.TALK);
	}

	@Override
	public String requestWhisper(Agent agent) {
		return (String) request(Request.WHISPER);
	}

	@Override
	public Agent requestVote(Agent agent) {
		return (Agent) request(Request.VOTE);
	}

	@Override
	public Agent requestDivineTarget(Agent agent) {
		return (Agent) request(Request.DIVINE);
	}

	@Override
	public Agent requestGuardTarget(Agent agent) {
		return (Agent) request(Request.GUARD);
	}

	@Override
	public Agent requestAttackTarget(Agent agent) {
		return (Agent) request(Request.ATTACK);
	}

	@Override
	public void finish(Agent agent) {
		send(Request.FINISH);
	}

	@Override
	public void setGameData(GameData gameData) {
		this.gameData = gameData;
	}

	@Override
	public void setGameSetting(GameSetting gameSetting) {
		this.gameSetting = gameSetting;
	}

	@Override
	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void usage() {
		System.err.println("Usage: " + PhantomServer.class.getSimpleName() + " [-p port] [-i logFile]");
	}

	// TODO CustomSetting
	public static void main(String[] args) throws SocketTimeoutException, IOException {
		int port = 10000;
		String logFileName = null;

		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) {
				if (args[i].equals("-p")) {
					i++;
					port = Integer.parseInt(args[i]);
					if (port < 0) {
						port = 10000;
					}
				} else if (args[i].equals("-i")) {
					i++;
					logFileName = args[i];
				} else if (args[i].equals("-h")) {
					usage();
					System.exit(0);
				}
			}
		}

		InputStream stream = logFileName != null ? new FileInputStream(logFileName) : System.in;

		new PhantomGame(stream, port);
	}

}
