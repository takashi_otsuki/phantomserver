# PhantomServer

PhantomServer is a virtual AIWolf game server
which accepts only one connection from agent.
It reads AIWolf game log,
and acts as if the all agents played according to the log.
The connected agent is assigned to the specified one in the log,
in other words,
it receives the same information
as the specified one.
However,
what it sends to the server is ignored
as if it were a phantom.

## Usage

` java -jar PhantomServer.jar [-p port] [-i logFile]`

PhantomServer waits a connection from the agent to port `port`.
In case that `port` is not specified, the server waits at port 10000.

The server reads log from `logFile`.
In case that `logFile` is not specified, it reads from standard input stream.
This means that multiple game logs can be read using pipe.

If the name of the connected agent exists in the log,
the agent is assigned to the one with the same name.
Otherwise, if the connected agent requests its role,
it is assigned to the one of the agent with the same role at random.
In case of no requested role,
the agent is assigned to the one of the agent in the log at random.

---

PhantomServerは一つのエージェントからの接続しか受け付けない仮想的な人狼知能ゲームサーバです．
PhantomServerは人狼知能のゲームログを読み込み，
あたかもエージェントたちがログに従ってプレイしているかのように動作します．
接続されたエージェントは，ログ中の指定されたエージェントに割り当てられます．
これはつまり，指定されたエージェントと同じ情報を受け取るということです．
しかし，エージェントからサーバに送られたものは，
まるでエージェントが幽霊であるかのようにすべて無視されます．

## 使用法

` java -jar PhantomServer.jar [-p port] [-i logFile]`

PhantomServerはエージェントからポート`port`への接続を待ちます．
`port`が指定されなかった場合は10000番ポートになります．

PhantomServerは`logFile`からログを読み込みます．
`logFile`が指定されなかった場合は標準入力から読み込みますので，
複数のログファイルをパイプを使って読ませることが可能です．

接続されたエージェントの名前がログ中に存在する場合，
エージェントは同じ名前のエージェントに割り当てられます．
ログ中に名前がなかった場合，
エージェントが役職をリクエストしていれば
ログ中の同じ役職のエージェントの一つにランダムに割り当てられ，
役職をリクエストしていなければ
ログ中のエージェントの一つにランダムに割り当てられます．